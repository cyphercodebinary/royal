/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.controlDeMantenimiento.model;

/**
 * 
 * @author MarcoPG <McCoy :v >
 */
public class Product {

    
    private String id_Product;
    private String id_Hotel;
    private String nombre;
    private String cantidad;
    private String descipcion;
    
    public Product(){
 
    }

    public Product(String id_Hotel, String nombre, String cantidad, String descipcion) {
        this.id_Hotel = id_Hotel;
        this.nombre = nombre;
        this.cantidad = cantidad;
        this.descipcion = descipcion;
    }

    public Product(String nombre, String cantidad, String descipcion) {
        this.nombre = nombre;
        this.cantidad = cantidad;
        this.descipcion = descipcion;
    }

    public Product(String id_Product) {
        this.id_Product = id_Product;
    }
    

    public Product(String id_Product, String id_Hotel, String nombre, String cantidad, String descipcion) {
        this.id_Product = id_Product;
        this.id_Hotel = id_Hotel;
        this.nombre = nombre;
        this.cantidad = cantidad;
        this.descipcion = descipcion;
    }

    public String getId_Product() {
        return id_Product;
    }

    public void setId_Product(String id_Product) {
        this.id_Product = id_Product;
    }

    public String getId_Hotel() {
        return id_Hotel;
    }

    public void setId_Hotel(String id_Hotel) {
        this.id_Hotel = id_Hotel;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCantidad() {
        return cantidad;
    }

    public void setCantidad(String cantidad) {
        this.cantidad = cantidad;
    }

    public String getDescipcion() {
        return descipcion;
    }

    public void setDescipcion(String descipcion) {
        this.descipcion = descipcion;
    }
    
    
    
    
    
    
    
    
}

