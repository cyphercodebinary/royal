/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.controlDeManteniemiento.dao;

import com.controlDeMantenimiento.model.Product;
import java.util.ArrayList;

/**
 *
 * @author MarcoPG <McCoy :v >
 */
public interface invantarioDao {
    
    String insertProduct(int id_hotel,String nombre,int cantidad,String desc);
    
    String deleteProduct(Product eliminarProduct);
    
    String updateProduct(Product actualizarProduct);
    
    ArrayList<Product> getProducts();
    
    
    
}
