/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.controlDeManteniemiento.dao;

import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * 
 * @author Sammy Guergachi <sguergachi at gmail.com>
 */
public class MysqlSingleton {

    public com.mysql.jdbc.Connection conn;
    public Statement statement;
    public static MysqlSingleton db;
    
    public MysqlSingleton() {
     try{
           Class.forName("com.mysql.jdbc.Driver").newInstance();
           
           String server="jdbc:mysql://localhost:8080/pruebaRoyal";
           String userName = "root"; 
           String password = "";
           
           this.conn=(com.mysql.jdbc.Connection) DriverManager.getConnection(server, userName, password);
       }catch(SQLException ex){
           System.out.println("sql error"+ex.getMessage());
           
       }catch(ClassNotFoundException ex){
           System.out.println("Driver not found"+ ex.getMessage());
       }catch(InstantiationException ex){
           System.out.println("new instance error"+ ex.getMessage());
       }catch(IllegalAccessException ex){
           System.out.println("illegal Acces"+ ex.getMessage());
       }
    }
    
    public static MysqlSingleton getInstance() {
        if (db==null) {
            db= new MysqlSingleton();
        }
        return db;
    }
    
    private static class mysqlSingletonHolder {

        private static final MysqlSingleton INSTANCE = new MysqlSingleton();
    }
}
