/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.controlDeMantenimiento.controller;

import com.controlDeMantenimiento.model.Product;
import java.util.ArrayList;
import com.controlDeManteniemiento.dao.inventarioDaoImpl;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 * 
 * @author MarcoPG <McCoy :v >
 */
public class ControllerProducto {

    private ArrayList<Product> product;
    
    public ControllerProducto(){
        product = new ArrayList<>();
    }
    inventarioDaoImpl impl = new inventarioDaoImpl();
    
    public String insertProduct(String id_hotel, String nombre, String cantidad, String descripccion){
        if (id_hotel.isEmpty() || nombre.isEmpty() || cantidad.isEmpty() || descripccion.isEmpty()) {
            
        }
        Product product = new Product(id_hotel,nombre,cantidad,descripccion);
        String serveAnswer = impl.insertProduct(0, nombre, 0, descripccion);
        return serveAnswer;
        
    }
    public  String updateProduct(String nombre, String cantidad, String descipcion){
        if (nombre.isEmpty() || cantidad.isEmpty() || descipcion.isEmpty()) {
            JOptionPane.showMessageDialog(null, "No se pudo actualizar el producto");
        }
        
        Product product = new Product(nombre, cantidad, descipcion);
        String serveAnswer = impl.updateProduct(product);
        return serveAnswer;
        
    }
    
    public String eliminarProducto(String id){
        if (id.isEmpty()) {
            JOptionPane.showMessageDialog(null, "No se pudo eliminar el producto");
        }
        Product product = new Product(id);
        String serveAnswer = impl.deleteProduct(product);
        return serveAnswer;
    }
    
    public void fillProduct(JTable jTableproduct){
        DefaultTableModel model = new DefaultTableModel();
        jTableproduct.setModel(model);
        model.addColumn("Clave");
        model.addColumn("Hotel");
        model.addColumn("Nombre");
        model.addColumn("Cantidad");
        model.addColumn("Descripción");
        
        Object[] columns = new Object[5];
        ArrayList<Product> listOfProduct = impl.getProducts();
        int numAffectedRows = listOfProduct.size();
        for (int i = 0; i < numAffectedRows; i++) {
            columns[0] = listOfProduct.get(i).getId_Product();
            columns[1] = listOfProduct.get(i).getId_Hotel();
            columns[2] = listOfProduct.get(i).getNombre();
            columns[3] = listOfProduct.get(i).getCantidad();
            columns[4] = listOfProduct.get(i).getDescipcion();
            model.addRow(columns);
        }
    }
    
}
