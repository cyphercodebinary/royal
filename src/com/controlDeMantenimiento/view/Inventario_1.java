/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.controlDeMantenimiento.view;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * 
 * @author MarcoPG <McCoy :v >
 */
@Entity
@Table(name = "inventario", catalog = "inventario", schema = "")
@NamedQueries({
    @NamedQuery(name = "Inventario_1.findAll", query = "SELECT i FROM Inventario_1 i")
    , @NamedQuery(name = "Inventario_1.findById", query = "SELECT i FROM Inventario_1 i WHERE i.id = :id")
    , @NamedQuery(name = "Inventario_1.findByProducto", query = "SELECT i FROM Inventario_1 i WHERE i.producto = :producto")
    , @NamedQuery(name = "Inventario_1.findByPrecio", query = "SELECT i FROM Inventario_1 i WHERE i.precio = :precio")
    , @NamedQuery(name = "Inventario_1.findByStock", query = "SELECT i FROM Inventario_1 i WHERE i.stock = :stock")})
public class Inventario_1 implements Serializable {

    @Transient
    private PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "producto")
    private String producto;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "precio")
    private Double precio;
    @Column(name = "stock")
    private Integer stock;

    public Inventario_1() {
    }

    public Inventario_1(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        Integer oldId = this.id;
        this.id = id;
        changeSupport.firePropertyChange("id", oldId, id);
    }

    public String getProducto() {
        return producto;
    }

    public void setProducto(String producto) {
        String oldProducto = this.producto;
        this.producto = producto;
        changeSupport.firePropertyChange("producto", oldProducto, producto);
    }

    public Double getPrecio() {
        return precio;
    }

    public void setPrecio(Double precio) {
        Double oldPrecio = this.precio;
        this.precio = precio;
        changeSupport.firePropertyChange("precio", oldPrecio, precio);
    }

    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        Integer oldStock = this.stock;
        this.stock = stock;
        changeSupport.firePropertyChange("stock", oldStock, stock);
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Inventario_1)) {
            return false;
        }
        Inventario_1 other = (Inventario_1) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.controlDeMantenimiento.view.Inventario_1[ id=" + id + " ]";
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.removePropertyChangeListener(listener);
    }

}
