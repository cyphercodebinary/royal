/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.controlDeManteniemiento.dao;

import com.controlDeMantenimiento.model.Product;
import com.mysql.jdbc.MySQLConnection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * 
 * @author MarcoPG <McCoy :v >
 */
public class inventarioDaoImpl implements invantarioDao{
    
    MysqlSingleton m = MysqlSingleton.db;


    
    public ArrayList<Product> listProduct(){
        ArrayList<Product> listProduct = new ArrayList<>();
        try {
            PreparedStatement statement = MysqlSingleton.getInstance().conn.prepareStatement("SELECT id,id_hotel,nombre,catidad,descripcion FROM ORDER BY id desc");
            ResultSet resultSet = statement.executeQuery();
            Product auxProduct;
            while (resultSet.next()){
                auxProduct = new Product();
                auxProduct.setId_Hotel(resultSet.getString(1));
                auxProduct.setNombre(resultSet.getString(2));
                auxProduct.setCantidad(resultSet.getString(3));
                auxProduct.setDescipcion(resultSet.getString(4));
                
                listProduct.add(auxProduct);
            }
        } catch (Exception e) {
            System.out.println("SQLException " + e.getMessage());
        }
        System.out.print(listProduct);
        return listProduct;
        
    }
    

    @Override
    public String insertProduct(int id_hotel, String nombre, int cantidad, String desc) {
        String serveAnswer = null;
        try {
            PreparedStatement statement = (PreparedStatement)MysqlSingleton.getInstance().conn.prepareStatement("INSERT INTO materiales_herramientas(id_hotel,nombre,cantidad,precio) VALUES(?,?,?,?)");
            
            statement.setInt(1, id_hotel);
            statement.setString(2, nombre);
            statement.setInt(3, cantidad);
            statement.setString(4, desc);
            int numaffectedRows = statement.executeUpdate();
            if (numaffectedRows > 0) {
                serveAnswer = "Articulo insertado correctamente";
            }
        } catch (Exception e) {
            
        }
        return null;
    }

    @Override
    public String updateProduct(Product actualizarProduct) {
        String serverAnswer = null;
        try {
            PreparedStatement statement = (PreparedStatement)MysqlSingleton.getInstance().conn.prepareStatement("UPDATE materiales_herramientas SET id_hotel=?,nombre=?,cantidad=?,descripcion=? WHERE id=?");
            statement.setString(1, actualizarProduct.getId_Hotel());
            statement.setString(2, actualizarProduct.getNombre());
            statement.setString(3, actualizarProduct.getCantidad());
            statement.setString(4, actualizarProduct.getDescipcion());
            int numAffectedRows = statement.executeUpdate();
            if (numAffectedRows > 0) {
                serverAnswer = "Producto alctualizado";
            }
        } catch (SQLException e) {
             System.out.println("sql error" + e.getMessage());
            return serverAnswer = "Ocurrio un problema";
        }
        return serverAnswer;
    }

    public ArrayList<Product> getProducts() {
        ArrayList<Product> listProducts = new ArrayList<>();
        String serverAnswer = null;
        try {
            PreparedStatement statement = (PreparedStatement)MysqlSingleton.getInstance().conn.prepareCall("SELECT * FROM materiales_herramientas order by id desc;");
            ResultSet resultSet = statement.executeQuery();
            Product auxProduct;
            while (resultSet.next()) {                
                auxProduct = new Product();
                auxProduct.setId_Product(resultSet.getString(1));
                auxProduct.setId_Hotel(resultSet.getString(2));
                auxProduct.setNombre(resultSet.getString(3));
                auxProduct.setCantidad(resultSet.getString(4));
                listProducts.add(auxProduct);
            }
        } catch (Exception e) {
            System.out.println("SQLException" + e.getMessage());
            serverAnswer = "Ocurrio un Problema!!!";
        }
        return listProducts;
    }

    @Override
    public String deleteProduct(Product eliminarProduct) {
       String serverAnswer = null;
        try {
            PreparedStatement statement = (PreparedStatement)MysqlSingleton.getInstance().conn.prepareStatement("DELETE from materiales_herramientas WHERE id=?");
            statement.setString(1, serverAnswer);
            int numAffectedRows = statement.executeUpdate();
            if (numAffectedRows > 0) {
                serverAnswer = "Se a eliminida correctamente el producto";
            }
        } catch (SQLException e) {
            System.out.println("sql error"+e.getMessage());
            return serverAnswer="Ocurrio un problema al eliminar el producto";
        }
        return serverAnswer;
        
    }
    
    
    

}
