/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.controlDeMantenimiento.model;

/**
 * 
 * @author MarcoPG <McCoy :v >
 */
public class Equipos {

    private String id_Equipo;
    private String id_Depa;
    private String desc;
    private String so;
    private String id_tipo;
    private String fecha_compra;
    private String id_Usuer;
    private String id_hotel;
    private String num_fact;

    public Equipos() {
    }

    public Equipos(String id_Equipo, String id_Depa, String desc, String so, String id_tipo, String fecha_compra, String id_Usuer, String id_hotel, String num_fact) {
        this.id_Equipo = id_Equipo;
        this.id_Depa = id_Depa;
        this.desc = desc;
        this.so = so;
        this.id_tipo = id_tipo;
        this.fecha_compra = fecha_compra;
        this.id_Usuer = id_Usuer;
        this.id_hotel = id_hotel;
        this.num_fact = num_fact;
    }

    public String getId_Equipo() {
        return id_Equipo;
    }

    public void setId_Equipo(String id_Equipo) {
        this.id_Equipo = id_Equipo;
    }

    public String getId_Depa() {
        return id_Depa;
    }

    public void setId_Depa(String id_Depa) {
        this.id_Depa = id_Depa;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getSo() {
        return so;
    }

    public void setSo(String so) {
        this.so = so;
    }

    public String getId_tipo() {
        return id_tipo;
    }

    public void setId_tipo(String id_tipo) {
        this.id_tipo = id_tipo;
    }

    public String getFecha_compra() {
        return fecha_compra;
    }

    public void setFecha_compra(String fecha_compra) {
        this.fecha_compra = fecha_compra;
    }

    public String getId_Usuer() {
        return id_Usuer;
    }

    public void setId_Usuer(String id_Usuer) {
        this.id_Usuer = id_Usuer;
    }

    public String getId_hotel() {
        return id_hotel;
    }

    public void setId_hotel(String id_hotel) {
        this.id_hotel = id_hotel;
    }

    public String getNum_fact() {
        return num_fact;
    }

    public void setNum_fact(String num_fact) {
        this.num_fact = num_fact;
    }
    
    
    
    
    
}
